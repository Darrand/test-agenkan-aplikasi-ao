import React, { useEffect, useState } from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';

// Please don't use React Class component like this

// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//         <div className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <h2>Welcome to React</h2>
//         </div>
//         <p className="App-intro">
//           To get started, edit <code>src/App.js</code> and save to reload.
//           <br></br>
//           This is supposed to be AgenKAN AO Productivity Monitoring Officer frontend.
//         </p>
//       </div>
//     );
//   }
// }

// Use React Functional component like this instead

// for environment purposes

const App = () => {
  const [apiResponse, setApiResponse] = useState('\
    API is NOT working properly. \
    Please make sure the server-side application has been started.');

  useEffect(() => {
    const url = process.env.REACT_APP_HOST;
    const fetchData = async () => {
      const res = await axios.get(url + 'testAPI')
        .then(res => setApiResponse(res.data.message),
          err => console.log(err.message))
    }
    fetchData();
  }, []);

  return (
    <div className="App">
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>Welcome to React</h2>
      </div>
      <p className="App-intro">
        To get started, edit <code>src/App.js</code> and save to reload.
      </p>
      <p>
        This is supposed to be AgenKAN AO Productivity Monitoring Officer frontend.
        <br></br>
        {apiResponse}
      </p>
    </div>
  )
}

// Read more: https://medium.com/@Zwenza/functional-vs-class-components-in-react-231e3fbd7108

export default App;
