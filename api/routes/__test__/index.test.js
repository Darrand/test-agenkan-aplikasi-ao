const app = require('../../app');
const request = require('supertest');

/* tests the availability of the home page */
it('Gets through the home page', async () => {
    const response = await request(app).get('/')
    expect(response.status).toBe(200)
}) 
