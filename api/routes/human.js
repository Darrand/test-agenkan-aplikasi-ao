var express = require('express');
var router = express.Router();

const humanController = require('../controllers').human;

router.get('/', humanController.list);
router.post('/', humanController.add);

module.exports = router; 