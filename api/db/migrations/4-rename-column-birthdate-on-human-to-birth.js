'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "birthdate" from table "human"
 * addColumn "birth" to table "human"
 *
 **/

var info = {
    "revision": 4,
    "name": "rename-column-birthdate-on-human-to-birth",
    "created": "2020-02-10T16:05:10.907Z",
    "comment": ""
};

var migrationCommands = function (transaction) {
    return [
        // {
        //     fn: "removeColumn",
        //     params: [
        //         "human",
        //         "birthdate",
        //         {
        //             transaction: transaction
        //         }
        //     ]
        // },
        // {
        //     fn: "addColumn",
        //     params: [
        //         "human",
        //         "birth",
        //         {
        //             "type": Sequelize.DATE,
        //             "field": "birth"
        //         },
        //         {
        //             transaction: transaction
        //         }
        //     ]
        // }

        // use rename instead of remove and add
        {
            fn: "renameColumn",
            params: [
                "human",
                "birthdate",
                "birth",
                {
                    transaction: transaction
                }
            ]
        },
    ];
};
var rollbackCommands = function (transaction) {
    return [
        // {
        //     fn: "removeColumn",
        //     params: [
        //         "human",
        //         "birth",
        //         {
        //             transaction: transaction
        //         }
        //     ]
        // },
        // {
        //     fn: "addColumn",
        //     params: [
        //         "human",
        //         "birthdate",
        //         {
        //             "type": Sequelize.DATE,
        //             "field": "birthdate"
        //         },
        //         {
        //             transaction: transaction
        //         }
        //     ]
        // }
        {
            fn: "renameColumn",
            params: [
                "human",
                "birth",
                "birthdate",
                {
                    transaction: transaction
                }
            ]
        },
    ];
};

module.exports = {
    pos: 0,
    useTransaction: true,
    execute: function (queryInterface, Sequelize, _commands) {
        var index = this.pos;
        function run(transaction) {
            const commands = _commands(transaction);
            return new Promise(function (resolve, reject) {
                function next() {
                    if (index < commands.length) {
                        let command = commands[index];
                        console.log("[#" + index + "] execute: " + command.fn);
                        index++;
                        queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                    }
                    else
                        resolve();
                }
                next();
            });
        }
        if (this.useTransaction) {
            return queryInterface.sequelize.transaction(run);
        } else {
            return run(null);
        }
    },
    up: function (queryInterface, Sequelize) {
        return this.execute(queryInterface, Sequelize, migrationCommands);
    },
    down: function (queryInterface, Sequelize) {
        return this.execute(queryInterface, Sequelize, rollbackCommands);
    },
    info: info
};
