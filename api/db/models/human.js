'use strict';
module.exports = (sequelize, DataTypes) => {
  const human = sequelize.define('human', {
    name: DataTypes.STRING,
    birth: DataTypes.DATE,
    age: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
    // allowNull: false
    // isMarried: DataTypes.BOOLEAN,
    // description: DataTypes.TEXT,
    // salary: DataTypes.FLOAT
  }, {
    freezeTableName: true
  });
  human.associate = function (models) {
    // associations can be defined here
  };
  return human;
};