const Human = require('../db/models').human;

module.exports = {
    list(req, res) {
        return Human
            .findAll({
                include: [],
                order: [
                    ['createdAt'],
                ],
            })
            .then((humans) => res.status(200).send(humans))
            .catch((error) => { res.status(400).send(error); });
    },
    add(req, res) {
        console.log(req.body);
        return Human
            .create({
                name: req.body.name,
                birth: req.body.birth,
                age: req.body.age
            })
            .then((human) => res.status(201).send(human))
            .catch((error) => res.status(400).send(error));
    },
}